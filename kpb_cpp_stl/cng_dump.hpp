#ifndef _H_CNG_DUMP_CPP_H_
#define _H_CNG_DUMP_CPP_H_

#include <cng_ioctl_in.hpp>
#include <cng_ioctl_out.hpp>

namespace cng {

void dump_ioctl_in(const cng::CngIoctlIn &ioctl);
void dump_ioctl_out(const cng::CngIoctlOut &ioctl);

};

#endif // _H_CNG_DUMP_CPP_H_
