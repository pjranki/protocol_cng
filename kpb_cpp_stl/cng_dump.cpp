#include <cng_dump.hpp>

#include <iostream>

static void hexdumpline(size_t offset, void *buf, size_t size)
{
    char line[128];
    char *data = (char*)buf;
    char *pos = &(line[0]);
    size_t i;

    memset(line, 0, sizeof(line));

    if (size > 16)
    {
        size = 16;
    }

    for (i = 0; i < size; i++)
    {
        snprintf(pos, 4, "%02x ", (unsigned char)(data[i]));
        pos += 3;
    }
    for (i = 0; i < (16 - size); i++)
    {
        memcpy(pos, "   ", 3);
        pos += 3;
    }
    memcpy(pos, "| ", 2);
    pos += 2;
    for (i = 0; i < size; i++)
    {
        if ((data[i] >= ' ') && (data[i] <= '~'))
        {
            *pos = data[i];
        }
        else
        {
            *pos = '.';
        }
        pos += 1;
    }
    printf("%08x %s\n", (int) offset, line);
}

static void hexdump(const void *buf, size_t size)
{
    size_t i;
    char *data;

    for (i = 0; i < size; i += 16)
    {
        data = ((char*)buf) + i;
        if ((i + 16) <= size)
        {
            hexdumpline(i, data, 16);
        }
        else
        {
            hexdumpline(i, data, size - i);
        }
    }
}

namespace cng {

void dump_encrypt_memory(const cng::EncryptMemory &encrypt_memory)
{
    const char *encrypt_str = encrypt_memory.encrypt() ? "ENCRYPT" : "DECRYPT";
    const char *cipher_str = "";
    switch ( encrypt_memory.cipher() )
    {
        case cng::EncryptMemory::CIPHER_CBC:
            cipher_str = "CBC";
            break;
    
        case cng::EncryptMemory::CIPHER_AES:
            cipher_str = "AES";
            break;

        default:
            cipher_str = "ERROR";
            break;
    }
    const char *option_str = "";
    switch ( encrypt_memory.option() )
    {
        case cng::EncryptMemory::ENCRYPT_OPTION_PROCESS:
            option_str = "PROCESS";
            break;

        case cng::EncryptMemory::ENCRYPT_OPTION_GLOBAL:
            option_str = "GLOBAL";
            break;

        case cng::EncryptMemory::ENCRYPT_OPTION_AUTH_ID:
            option_str = "AUTH_ID";
            break;

        case cng::EncryptMemory::ENCRYPT_OPTION_AUTH_ID_994:
            option_str = "AUTH_ID_994";
            break;

        default:
            option_str = "UNKNOWN";
            break;
    }
    printf("%s MEMORY %s USING %s KEY (size: %d)\n", encrypt_str, cipher_str, option_str, (int)encrypt_memory.data().size());
    hexdump(encrypt_memory.data().c_str(), encrypt_memory.data().size());
}

void dump_system_prng(const cng::SystemPrng &system_prng)
{
    if ( 0 == system_prng.data().size() )
    {
        printf("SYSTEM PRNG\n");
        return;
    }
    printf("SYSTEM PRNG (size: %d)\n", (int)system_prng.data().size());
    hexdump(system_prng.data().c_str(), system_prng.data().size());
}

void dump_padding(const std::string &prefix, const std::string &padding)
{
    bool dump_padding = false;
    for ( size_t i = 0; i < padding.size(); i++ )
    {
        if ( padding[i] != 0x00 )
        {
            dump_padding = true;
            break;
        }
    }
    if ( dump_padding )
    {
        printf("%s.PADDING: (size %d):\n", prefix.c_str(), (int)padding.size());
        hexdump(padding.c_str(), padding.size());
    }
}

void dump_ptr(const std::string &prefix, const size_t &pointer_size, const uint64_t &ptr)
{
    if ( pointer_size == 8 )
    {
        printf("%s.Offset: 0x%08x%08x\n", prefix.c_str(), (uint32_t)(ptr >> 32), (uint32_t)(ptr >> 0));
    }
    else if ( pointer_size == 4 )
    {
        printf("%s.Offset: 0x%08x\n", prefix.c_str(), (uint32_t)ptr);
    }
    else
    {
        printf("%s.Offset: (unknown ptr size)\n", prefix.c_str());
    }
}

void dump_io_string_ptr(const std::string &prefix, const cng::IoStringPtr &io_string_ptr)
{
    if ( io_string_ptr.valid_offset() )
    {
        dump_ptr(prefix, io_string_ptr.pointer_size_(), io_string_ptr.offset());
        printf("%s.WStr: '%S'\n", prefix.c_str(), io_string_ptr.value().c_str());
    }
    else
    {
        printf("%s: (not set)\n", prefix.c_str());
    }
}

void dump_io_string_array(const std::string &prefix, const cng::IoStringArray &io_string_array)
{
    size_t i = 0;
    dump_padding(prefix, io_string_array.padding());
    if ( io_string_array.valid_offset() )
    {
        dump_ptr(prefix, io_string_array.pointer_size_(), io_string_array.offset());
    }
    else
    {
        printf("%s: (not set)\n", prefix.c_str());
    }
    for ( auto _io_string_ptr : io_string_array.array() )
    {
        auto &io_string_ptr = _io_string_ptr.get();
        dump_io_string_ptr(prefix + "[" + std::to_string(i++) + "]", io_string_ptr);
    }
}

void dump_io_buffer(const std::string &prefix, const cng::IoBuffer &io_buffer)
{
    dump_padding(prefix, io_buffer.padding());
    if ( io_buffer.valid_offset() )
    {
        dump_ptr(prefix, io_buffer.pointer_size_(), io_buffer.offset());
        printf("%s.Buffer: (size %d):\n", prefix.c_str(), (int)io_buffer.value().size());
        hexdump(io_buffer.value().c_str(), io_buffer.value().size());
    }
    else
    {
        printf("%s: (not set)\n", prefix.c_str());
    }
}

void dump_property_value(const std::string &prefix, const cng::PropertyValue &property_value)
{
    if ( property_value.value().size() > 0 )
    {
        printf("%s.Buffer: (size %d):\n", prefix.c_str(), (int)property_value.value().size());
        hexdump(property_value.value().c_str(), property_value.value().size());
    }
    else
    {
        printf("%s.Buffer: (empty)\n", prefix.c_str());
    }
}

void dump_crypt_image_ref(const std::string &prefix, const cng::CryptImageRef &crypt_image_ref)
{
    dump_io_string_ptr(prefix + ".Image", crypt_image_ref.image());
    printf("%s.Flags: 0x%08x\n", prefix.c_str(), crypt_image_ref.flags());
    dump_padding(prefix, crypt_image_ref.padding());
}

void dump_crypt_image_ref_ptr(const std::string &prefix, const cng::CryptImageRefPtr &crypt_image_ref_ptr)
{
    if ( crypt_image_ref_ptr.valid_offset() )
    {
        dump_ptr(prefix, crypt_image_ref_ptr.pointer_size_(), crypt_image_ref_ptr.offset());
        dump_crypt_image_ref(prefix, crypt_image_ref_ptr.value());
    }
    else
    {
        printf("%s: (not set)\n", prefix.c_str());
    }
}

void dump_crypt_property_ref(const std::string &prefix, const cng::CryptPropertyRef &crypt_property_ref)
{
    dump_io_string_ptr(prefix + ".Property", crypt_property_ref.property());
    dump_io_buffer(prefix + ".Value", crypt_property_ref.value());
}

void dump_crypt_property_ref_ptr(const std::string &prefix, const cng::CryptPropertyRefPtr &crypt_property_ref_ptr)
{
    if ( crypt_property_ref_ptr.valid_offset() )
    {
        dump_ptr(prefix, crypt_property_ref_ptr.pointer_size_(), crypt_property_ref_ptr.offset());
        dump_crypt_property_ref(prefix, crypt_property_ref_ptr.value());
    }
    else
    {
        printf("%s: (not set)\n", prefix.c_str());
    }
}

void dump_crypt_property_refs(const std::string &prefix, const cng::CryptPropertyRefs &crypt_property_refs)
{
    size_t i = 0;
    dump_padding(prefix, crypt_property_refs.padding());
    if ( crypt_property_refs.valid_offset() )
    {
        dump_ptr(prefix, crypt_property_refs.pointer_size_(), crypt_property_refs.offset());
    }
    else
    {
        printf("%s: (not set)\n", prefix.c_str());
    }
    for ( auto _crypt_property_ref_ptr : crypt_property_refs.array() )
    {
        auto &crypt_property_ref_ptr = _crypt_property_ref_ptr.get();
        dump_crypt_property_ref_ptr(prefix + "[" + std::to_string(i++) + "]", crypt_property_ref_ptr);
    }
}

void dump_crypt_provider_ref(const std::string &prefix, const cng::CryptProviderRef &crypt_provider_ref)
{
    printf("%s.Interface: 0x%x\n", prefix.c_str(), crypt_provider_ref.interface());
    dump_padding(prefix, crypt_provider_ref.padding());
    dump_io_string_ptr(prefix + ".Function", crypt_provider_ref.function());
    dump_io_string_ptr(prefix + ".Provider", crypt_provider_ref.provider());
    dump_crypt_property_refs(prefix + ".Property", crypt_provider_ref.properties());
    dump_crypt_image_ref_ptr(prefix + ".UM", crypt_provider_ref.um());
    dump_crypt_image_ref_ptr(prefix + ".KM", crypt_provider_ref.km());
}

void dump_crypt_provider_ref_ptr(const std::string &prefix, const cng::CryptProviderRefPtr &crypt_provider_ref_ptr)
{
    if ( crypt_provider_ref_ptr.valid_offset() )
    {
        dump_ptr(prefix, crypt_provider_ref_ptr.pointer_size_(), crypt_provider_ref_ptr.offset());
        dump_crypt_provider_ref(prefix, crypt_provider_ref_ptr.value());
    }
    else
    {
        printf("%s: (not set)\n", prefix.c_str());
    }
}

void dump_crypt_provider_refs(const std::string &prefix, const cng::CryptProviderRefs &crypt_provider_refs)
{
    size_t i = 0;
    dump_padding(prefix, crypt_provider_refs.padding());
    if ( crypt_provider_refs.valid_offset() )
    {
        dump_ptr(prefix, crypt_provider_refs.pointer_size_(), crypt_provider_refs.offset());
    }
    else
    {
        printf("%s: (not set)\n", prefix.c_str());
    }
    for ( auto _crypt_provider_ref_ptr : crypt_provider_refs.array() )
    {
        auto &crypt_provider_ref_ptr = _crypt_provider_ref_ptr.get();
        dump_crypt_provider_ref_ptr(prefix + "[" + std::to_string(i++) + "]", crypt_provider_ref_ptr);
    }
}

void dump_crypt_context_config(const std::string &prefix, const cng::CryptContextConfig &crypt_context_config)
{
    printf("%s.Flags: 0x%x\n", prefix.c_str(), crypt_context_config.flags());
    printf("%s.Reserved: 0x%x\n", prefix.c_str(), crypt_context_config.reserved());
}

void dump_crypt_context_config_ptr(const std::string &prefix, const cng::CryptContextConfigPtr &crypt_context_config_ptr)
{
    if ( crypt_context_config_ptr.valid_offset() )
    {
        dump_ptr(prefix, crypt_context_config_ptr.pointer_size_(), crypt_context_config_ptr.offset());
        dump_crypt_context_config(prefix, crypt_context_config_ptr.value());
    }
    else
    {
        printf("%s: (not set)\n", prefix.c_str());
    }
}

void dump_crypt_context_function_config(const std::string &prefix, const cng::CryptContextFunctionConfig &crypt_context_function_config)
{
    printf("%s.Flags: 0x%x\n", prefix.c_str(), crypt_context_function_config.flags());
    printf("%s.Reserved: 0x%x\n", prefix.c_str(), crypt_context_function_config.reserved());
}

void dump_crypt_context_function_config_ptr(const std::string &prefix, const cng::CryptContextFunctionConfigPtr &crypt_context_function_config_ptr)
{
    if ( crypt_context_function_config_ptr.valid_offset() )
    {
        dump_ptr(prefix, crypt_context_function_config_ptr.pointer_size_(), crypt_context_function_config_ptr.offset());
        dump_crypt_context_function_config(prefix, crypt_context_function_config_ptr.value());
    }
    else
    {
        printf("%s: (not set)\n", prefix.c_str());
    }
}

void dump_crypt_interface_reg(const std::string &prefix, const cng::CryptInterfaceReg &crypt_interface_reg)
{
    printf("%s.Interface: 0x%x\n", prefix.c_str(), crypt_interface_reg.interface());
    printf("%s.Flags: 0x%x\n", prefix.c_str(), crypt_interface_reg.flags());
    dump_io_string_array(prefix + ".Functions", crypt_interface_reg.functions());
}

void dump_crypt_interface_reg_ptr(const std::string &prefix, const cng::CryptInterfaceRegPtr &crypt_interface_reg_ptr)
{
    if ( crypt_interface_reg_ptr.valid_offset() )
    {
        dump_ptr(prefix, crypt_interface_reg_ptr.pointer_size_(), crypt_interface_reg_ptr.offset());
        dump_crypt_interface_reg(prefix, crypt_interface_reg_ptr.value());
    }
    else
    {
        printf("%s: (not set)\n", prefix.c_str());
    }
}

void dump_crypt_interface_regs(const std::string &prefix, const cng::CryptInterfaceRegs &crypt_interface_regs)
{
    size_t i = 0;
    dump_padding(prefix, crypt_interface_regs.padding());
    if ( crypt_interface_regs.valid_offset() )
    {
        dump_ptr(prefix, crypt_interface_regs.pointer_size_(), crypt_interface_regs.offset());
    }
    else
    {
        printf("%s: (not set)\n", prefix.c_str());
    }
    for ( auto _crypt_interface_reg_ptr : crypt_interface_regs.array() )
    {
        auto &crypt_interface_reg_ptr = _crypt_interface_reg_ptr.get();
        dump_crypt_interface_reg_ptr(prefix + "[" + std::to_string(i++) + "]", crypt_interface_reg_ptr);
    }
}

void dump_crypt_image_reg(const std::string &prefix, const cng::CryptImageReg &crypt_image_reg)
{
    dump_io_string_ptr(prefix + ".Image", crypt_image_reg.image());
    dump_crypt_interface_regs(prefix + ".Interfaces", crypt_image_reg.interfaces());
}

void dump_crypt_image_reg_ptr(const std::string &prefix, const cng::CryptImageRegPtr &crypt_image_reg_ptr)
{
    if ( crypt_image_reg_ptr.valid_offset() )
    {
        dump_ptr(prefix, crypt_image_reg_ptr.pointer_size_(), crypt_image_reg_ptr.offset());
        dump_crypt_image_reg(prefix, crypt_image_reg_ptr.value());
    }
    else
    {
        printf("%s: (not set)\n", prefix.c_str());
    }
}

void dump_crypt_provider_reg(const std::string &prefix, const cng::CryptProviderReg &crypt_provider_reg)
{
    dump_io_string_array(prefix + ".Aliases", crypt_provider_reg.aliases());
    dump_crypt_image_reg_ptr(prefix + ".UM", crypt_provider_reg.um());
    dump_crypt_image_reg_ptr(prefix + ".KM", crypt_provider_reg.km());
}

void dump_crypt_provider_reg_ptr(const std::string &prefix, const cng::CryptProviderRegPtr &crypt_provider_reg_ptr)
{
    if ( crypt_provider_reg_ptr.valid_offset() )
    {
        dump_ptr(prefix, crypt_provider_reg_ptr.pointer_size_(), crypt_provider_reg_ptr.offset());
        dump_crypt_provider_reg(prefix, crypt_provider_reg_ptr.value());
    }
    else
    {
        printf("%s: (not set)\n", prefix.c_str());
    }
}

void dump_io_registration_paramblock(const std::string &prefix, const cng::IoRegistrationParamblock &io_registration_paramblock)
{
    dump_io_string_ptr(prefix + ".IoRegistrationParamblock.Context", io_registration_paramblock.provider());
    printf("%s.IoRegistrationParamblock.Mode: %d\n", prefix.c_str(), io_registration_paramblock.mode());
    printf("%s.IoRegistrationParamblock.Interface: 0x%x\n", prefix.c_str(), io_registration_paramblock.interface());
    printf("%s.IoRegistrationParamblock.Flags: 0x%x\n", prefix.c_str(), io_registration_paramblock.flags());
    dump_padding(prefix + ".IoRegistrationParamblock", io_registration_paramblock.padding());
    dump_crypt_provider_reg_ptr(prefix + ".IoRegistrationParamblock.Reg", io_registration_paramblock.reg());
}

void dump_io_configuration_paramblock(const std::string &prefix, const cng::IoConfigurationParamblock &io_configuration_paramblock)
{
    printf("%s.IoConfigurationParamblock.Table: 0x%x\n", prefix.c_str(), io_configuration_paramblock.table());
    dump_padding(prefix + ".IoConfigurationParamblock", io_configuration_paramblock.padding1());
    dump_io_string_ptr(prefix + ".IoConfigurationParamblock.Context", io_configuration_paramblock.context());
    printf("%s.IoConfigurationParamblock.Interface: 0x%x\n", prefix.c_str(), io_configuration_paramblock.interface());
    dump_padding(prefix + ".IoConfigurationParamblock", io_configuration_paramblock.padding2());
    dump_io_string_ptr(prefix + ".IoConfigurationParamblock.Function", io_configuration_paramblock.function());
    dump_io_string_ptr(prefix + ".IoConfigurationParamblock.Provider", io_configuration_paramblock.provider());
    dump_io_string_ptr(prefix + ".IoConfigurationParamblock.Property", io_configuration_paramblock.property());
    printf("%s.IoConfigurationParamblock.Position: 0x%x\n", prefix.c_str(), io_configuration_paramblock.position());
    dump_padding(prefix + ".IoConfigurationParamblock", io_configuration_paramblock.padding3());
    dump_crypt_context_config_ptr(prefix + ".IoConfigurationParamblock.ContextConfig", io_configuration_paramblock.context_config());
    dump_crypt_context_function_config_ptr(prefix + ".IoConfigurationParamblock.ContextFunctionConfig", io_configuration_paramblock.context_function_config());
    dump_io_buffer(prefix + ".IoConfigurationParamblock.Value", io_configuration_paramblock.value());
    printf("%s.IoConfigurationParamblock.Event: 0x%p\n", prefix.c_str(), (void*)(size_t)io_configuration_paramblock.event());
}

void dump_io_resolution_paramblock(const std::string &prefix, const cng::IoResolutionParamblock &io_resolution_paramblock)
{
    dump_io_string_ptr(prefix + ".IoResolutionParamblock.Context", io_resolution_paramblock.context());
    printf("%s.IoResolutionParamblock.Interface: 0x%x\n", prefix.c_str(), io_resolution_paramblock.interface());
    dump_padding(prefix + ".IoResolutionParamblock", io_resolution_paramblock.padding());
    dump_io_string_ptr(prefix + ".IoResolutionParamblock.Function", io_resolution_paramblock.function());
    dump_io_string_ptr(prefix + ".IoResolutionParamblock.Provider", io_resolution_paramblock.provider());
    printf("%s.IoResolutionParamblock.Mode: %d\n", prefix.c_str(), io_resolution_paramblock.mode());
    printf("%s.IoResolutionParamblock.Flags: 0x%x\n", prefix.c_str(), io_resolution_paramblock.flags());
}

void dump_config_io_handler_in(const cng::ConfigIoHandlerIn &config_io_handler)
{
    printf("CONFIG IO HANDLER (IN)\n");
    switch( config_io_handler.function() )
    {
        case cng::ConfigIoHandlerIn::FUNCTION_BCRYPT_REGISTER_PROVIDER:
            dump_io_registration_paramblock("FUNCTION_BCRYPT_REGISTER_PROVIDER", config_io_handler.bcrypt_register_provider());
            break;

        case cng::ConfigIoHandlerIn::FUNCTION_BCRYPT_UNREGISTER_PROVIDER:
            dump_io_registration_paramblock("FUNCTION_BCRYPT_UNREGISTER_PROVIDER", config_io_handler.bcrypt_unregister_provider());
            break;

        case cng::ConfigIoHandlerIn::FUNCTION_BCRYPT_QUERY_PROVIDER_REGISTERATION:
            dump_io_registration_paramblock("FUNCTION_BCRYPT_QUERY_PROVIDER_REGISTERATION", config_io_handler.bcrypt_query_provider_registeration());
            break;

        case cng::ConfigIoHandlerIn::FUNCTION_BCRYPT_ENUM_REGISTERED_PROVIDERS:
            dump_io_registration_paramblock("FUNCTION_BCRYPT_ENUM_REGISTERED_PROVIDERS", config_io_handler.bcrypt_enum_registered_providers());
            break;

        case cng::ConfigIoHandlerIn::FUNCTION_BCRYPT_CREATE_CONTEXT:
            dump_io_configuration_paramblock("FUNCTION_BCRYPT_CREATE_CONTEXT", config_io_handler.bcrypt_create_context());
            break;

        case cng::ConfigIoHandlerIn::FUNCTION_BCRYPT_DELETE_CONTEXT:
            dump_io_configuration_paramblock("FUNCTION_BCRYPT_DELETE_CONTEXT", config_io_handler.bcrypt_delete_context());
            break;

        case cng::ConfigIoHandlerIn::FUNCTION_BCRYPT_ENUM_CONTEXTS:
            dump_io_configuration_paramblock("FUNCTION_BCRYPT_ENUM_CONTEXTS", config_io_handler.bcrypt_delete_context());
            break;

        case cng::ConfigIoHandlerIn::FUNCTION_BCRYPT_CONFIGURE_CONTEXT:
            dump_io_configuration_paramblock("FUNCTION_BCRYPT_CONFIGURE_CONTEXT", config_io_handler.bcrypt_configure_context());
            break;

        case cng::ConfigIoHandlerIn::FUNCTION_BCRYPT_QUERY_CONTEXT_CONFIGURATION:
            dump_io_configuration_paramblock("FUNCTION_BCRYPT_QUERY_CONTEXT_CONFIGURATION", config_io_handler.bcrypt_query_context_configuration());
            break;

        case cng::ConfigIoHandlerIn::FUNCTION_BCRYPT_ADD_CONTEXT_FUNCTION:
            dump_io_configuration_paramblock("FUNCTION_BCRYPT_ADD_CONTEXT_FUNCTION", config_io_handler.bcrypt_add_context_function());
            break;

        case cng::ConfigIoHandlerIn::FUNCTION_BCRYPT_REMOVE_CONTEXT_FUNCTION:
            dump_io_configuration_paramblock("FUNCTION_BCRYPT_REMOVE_CONTEXT_FUNCTION", config_io_handler.bcrypt_remove_context_function());
            break;

        case cng::ConfigIoHandlerIn::FUNCTION_BCRYPT_ENUM_CONTEXT_FUNCTIONS:
            dump_io_configuration_paramblock("FUNCTION_BCRYPT_ENUM_CONTEXT_FUNCTIONS", config_io_handler.bcrypt_enum_context_functions());
            break;

        case cng::ConfigIoHandlerIn::FUNCTION_BCRYPT_CONFIGURE_CONTEXT_FUNCTION:
            dump_io_configuration_paramblock("FUNCTION_BCRYPT_CONFIGURE_CONTEXT_FUNCTION", config_io_handler.bcrypt_configure_context_function());
            break;

        case cng::ConfigIoHandlerIn::FUNCTION_BCRYPT_QUERY_CONTEXT_FUNCTION_CONFIGURATION:
            dump_io_configuration_paramblock("FUNCTION_BCRYPT_QUERY_CONTEXT_FUNCTION_CONFIGURATION", config_io_handler.bcrypt_query_context_function_configuration());
            break;

        case cng::ConfigIoHandlerIn::FUNCTION_BCRYPT_ADD_CONTEXT_FUNCTION_PROVIDER:
            dump_io_configuration_paramblock("FUNCTION_BCRYPT_ADD_CONTEXT_FUNCTION_PROVIDER", config_io_handler.bcrypt_add_context_function_provider());
            break;

        case cng::ConfigIoHandlerIn::FUNCTION_BCRYPT_REMOVE_CONTEXT_FUNCTION_PROVIDER:
            dump_io_configuration_paramblock("FUNCTION_BCRYPT_REMOVE_CONTEXT_FUNCTION_PROVIDER", config_io_handler.bcrypt_remove_context_function_provider());
            break;

        case cng::ConfigIoHandlerIn::FUNCTION_BCRYPT_ENUM_CONTEXT_FUNCTION_PROVIDERS:
            dump_io_configuration_paramblock("FUNCTION_BCRYPT_ENUM_CONTEXT_FUNCTION_PROVIDERS", config_io_handler.bcrypt_enum_context_function_providers());
            break;

        case cng::ConfigIoHandlerIn::FUNCTION_BCRYPT_SET_CONTEXT_FUNCTION_PROPERTY:
            dump_io_configuration_paramblock("FUNCTION_BCRYPT_SET_CONTEXT_FUNCTION_PROPERTY", config_io_handler.bcrypt_set_context_function_property());
            break;

        case cng::ConfigIoHandlerIn::FUNCTION_BCRYPT_QUERY_CONTEXT_FUNCTION_PROPERTY:
            dump_io_configuration_paramblock("FUNCTION_BCRYPT_QUERY_CONTEXT_FUNCTION_PROPERTY", config_io_handler.bcrypt_query_context_function_property());
            break;

        case cng::ConfigIoHandlerIn::FUNCTION_CLIENT_REGISTER_CONFIG_CHANGE_NOTIFY:
            dump_io_configuration_paramblock("FUNCTION_CLIENT_REGISTER_CONFIG_CHANGE_NOTIFY", config_io_handler.client_register_config_change_notify());
            break;

        case cng::ConfigIoHandlerIn::FUNCTION_CLIENT_UNREGISTER_CONFIG_CHANGE_NOTIFY:
            dump_io_configuration_paramblock("FUNCTION_CLIENT_UNREGISTER_CONFIG_CHANGE_NOTIFY", config_io_handler.client_unregister_config_change_notify());
            break;

        case cng::ConfigIoHandlerIn::FUNCTION_BCRYPT_RESOLVE_PROVIDERS:
            dump_io_resolution_paramblock("FUNCTION_BCRYPT_RESOLVE_PROVIDERS", config_io_handler.bcrypt_resolve_providers());
            break;

        default:
            printf("(unknown function 0x%08x)\n", config_io_handler.function());
            break;
    }
    if ( config_io_handler.has_paramblock() )
    {
        hexdump(
            config_io_handler.paramblock().data().c_str(),
            config_io_handler.paramblock().data().size());
    }
}

void dump_config_io_handler_out(const cng::ConfigIoHandlerOut &config_io_handler)
{
    printf("CONFIG IO HANDLER (OUT)\n");
    if ( config_io_handler.nt_status() != 0 )
    {
        printf("NTSTATUS: 0x%08x\n", config_io_handler.nt_status());
        if ( config_io_handler.nt_status() == 0xc0000023 )
        {
            printf("REQUIRED SIZE: %d\n", config_io_handler.required_size_());
        }
        return;
    }
    switch( config_io_handler.function() )
    {
        case cng::ConfigIoHandlerOut::FUNCTION_BCRYPT_REGISTER_PROVIDER:
            printf("FUNCTION_BCRYPT_REGISTER_PROVIDER\n");
            break;

        case cng::ConfigIoHandlerOut::FUNCTION_BCRYPT_UNREGISTER_PROVIDER:
            printf("FUNCTION_BCRYPT_UNREGISTER_PROVIDER\n");
            break;

        case cng::ConfigIoHandlerOut::FUNCTION_BCRYPT_QUERY_PROVIDER_REGISTERATION:
            dump_crypt_provider_reg("FUNCTION_BCRYPT_QUERY_PROVIDER_REGISTERATION", config_io_handler.bcrypt_query_provider_registeration());
            break;

        case cng::ConfigIoHandlerOut::FUNCTION_BCRYPT_ENUM_REGISTERED_PROVIDERS:
            dump_io_string_array("FUNCTION_BCRYPT_ENUM_REGISTERED_PROVIDERS", config_io_handler.bcrypt_enum_registered_providers());
            break;

        case cng::ConfigIoHandlerOut::FUNCTION_BCRYPT_CREATE_CONTEXT:
            printf("FUNCTION_BCRYPT_CREATE_CONTEXT\n");
            break;

        case cng::ConfigIoHandlerOut::FUNCTION_BCRYPT_DELETE_CONTEXT:
            printf("FUNCTION_BCRYPT_DELETE_CONTEXT\n");
            break;

        case cng::ConfigIoHandlerOut::FUNCTION_BCRYPT_ENUM_CONTEXTS:
            dump_io_string_array("FUNCTION_BCRYPT_ENUM_CONTEXTS", config_io_handler.bcrypt_enum_contexts());
            break;

        case cng::ConfigIoHandlerOut::FUNCTION_BCRYPT_CONFIGURE_CONTEXT:
            printf("FUNCTION_BCRYPT_CONFIGURE_CONTEXT\n");
            break;

        case cng::ConfigIoHandlerOut::FUNCTION_BCRYPT_QUERY_CONTEXT_CONFIGURATION:
            dump_crypt_context_config("FUNCTION_BCRYPT_QUERY_CONTEXT_CONFIGURATION", config_io_handler.bcrypt_query_context_configuration());
            break;

        case cng::ConfigIoHandlerOut::FUNCTION_BCRYPT_ADD_CONTEXT_FUNCTION:
            printf("FUNCTION_BCRYPT_ADD_CONTEXT_FUNCTION\n");
            break;

        case cng::ConfigIoHandlerOut::FUNCTION_BCRYPT_REMOVE_CONTEXT_FUNCTION:
            printf("FUNCTION_BCRYPT_REMOVE_CONTEXT_FUNCTION\n");
            break;

        case cng::ConfigIoHandlerOut::FUNCTION_BCRYPT_ENUM_CONTEXT_FUNCTIONS:
            dump_io_string_array("FUNCTION_BCRYPT_ENUM_CONTEXT_FUNCTIONS", config_io_handler.bcrypt_enum_context_functions());
            break;

        case cng::ConfigIoHandlerOut::FUNCTION_BCRYPT_CONFIGURE_CONTEXT_FUNCTION:
            printf("FUNCTION_BCRYPT_CONFIGURE_CONTEXT_FUNCTION\n");
            break;

        case cng::ConfigIoHandlerOut::FUNCTION_BCRYPT_QUERY_CONTEXT_FUNCTION_CONFIGURATION:
            dump_crypt_context_function_config("FUNCTION_BCRYPT_QUERY_CONTEXT_FUNCTION_CONFIGURATION", config_io_handler.bcrypt_query_context_function_configuration());
            break;

        case cng::ConfigIoHandlerOut::FUNCTION_BCRYPT_ADD_CONTEXT_FUNCTION_PROVIDER:
            printf("FUNCTION_BCRYPT_ADD_CONTEXT_FUNCTION_PROVIDER\n");
            break;

        case cng::ConfigIoHandlerOut::FUNCTION_BCRYPT_REMOVE_CONTEXT_FUNCTION_PROVIDER:
            printf("FUNCTION_BCRYPT_REMOVE_CONTEXT_FUNCTION_PROVIDER\n");
            break;

        case cng::ConfigIoHandlerOut::FUNCTION_BCRYPT_ENUM_CONTEXT_FUNCTION_PROVIDERS:
            dump_io_string_array("FUNCTION_BCRYPT_ENUM_CONTEXT_FUNCTION_PROVIDERS", config_io_handler.bcrypt_enum_context_function_providers());
            break;

        case cng::ConfigIoHandlerOut::FUNCTION_BCRYPT_SET_CONTEXT_FUNCTION_PROPERTY:
            printf("FUNCTION_BCRYPT_SET_CONTEXT_FUNCTION_PROPERTY\n");
            break;

        case cng::ConfigIoHandlerOut::FUNCTION_BCRYPT_QUERY_CONTEXT_FUNCTION_PROPERTY:
            dump_property_value("FUNCTION_BCRYPT_QUERY_CONTEXT_FUNCTION_PROPERTY", config_io_handler.bcrypt_query_context_function_property());
            break;

        case cng::ConfigIoHandlerOut::FUNCTION_CLIENT_REGISTER_CONFIG_CHANGE_NOTIFY:
            printf("FUNCTION_CLIENT_REGISTER_CONFIG_CHANGE_NOTIFY\n");
            break;

        case cng::ConfigIoHandlerOut::FUNCTION_CLIENT_UNREGISTER_CONFIG_CHANGE_NOTIFY:
            printf("FUNCTION_CLIENT_UNREGISTER_CONFIG_CHANGE_NOTIFY\n");
            break;

        case cng::ConfigIoHandlerOut::FUNCTION_BCRYPT_RESOLVE_PROVIDERS:
            dump_crypt_provider_refs("FUNCTION_BCRYPT_RESOLVE_PROVIDERS.Providers", config_io_handler.bcrypt_resolve_providers());
            break;

        default:
            printf("(unknown function 0x%08x)\n", config_io_handler.function());
            break;
    }
    if ( config_io_handler.has_paramblock() )
    {
        hexdump(
            config_io_handler.paramblock().data().c_str(),
            config_io_handler.paramblock().data().size());
    }
}

void dump_ioctl_in(const cng::CngIoctlIn &ioctl)
{
    printf("CNG IOCTL 0x%08x (IN):\n", ioctl.ioctl());
    switch (ioctl.ioctl() )
    {
        case cng::CngIoctlIn::IOCTL_SYSTEM_PRNG:
            dump_system_prng(ioctl.system_prng());
            break;

        case cng::CngIoctlIn::IOCTL_SYSTEM_PRNG_2:
            dump_system_prng(ioctl.system_prng_2());
            break;

        case cng::CngIoctlIn::IOCTL_ENCRYPT_MEMORY_PROCESS:
            dump_encrypt_memory(ioctl.encrypt_memory_process());
            break;

        case cng::CngIoctlIn::IOCTL_DECRYPT_MEMORY_PROCESS:
            dump_encrypt_memory(ioctl.decrypt_memory_process());
            break;

        case cng::CngIoctlIn::IOCTL_ENCRYPT_MEMORY_GLOBAL:
            dump_encrypt_memory(ioctl.encrypt_memory_global());
            break;

        case cng::CngIoctlIn::IOCTL_DECRYPT_MEMORY_GLOBAL:
            dump_encrypt_memory(ioctl.decrypt_memory_global());
            break;

        case cng::CngIoctlIn::IOCTL_ENCRYPT_MEMORY_AUTH_ID:
            dump_encrypt_memory(ioctl.encrypt_memory_auth_id());
            break;

        case cng::CngIoctlIn::IOCTL_DECRYPT_MEMORY_AUTH_ID:
            dump_encrypt_memory(ioctl.decrypt_memory_auth_id());
            break;

        case cng::CngIoctlIn::IOCTL_ENCRYPT_MEMORY_AUTH_ID_994:
            dump_encrypt_memory(ioctl.encrypt_memory_auth_id());
            break;

        case cng::CngIoctlIn::IOCTL_DECRYPT_MEMORY_AUTH_ID_994:
            dump_encrypt_memory(ioctl.decrypt_memory_auth_id());
            break;

        case cng::CngIoctlIn::IOCTL_SELF_TEST_DURATION:
            printf("SELF TEST DURATION (IN)\n");
            break;

        case cng::CngIoctlIn::IOCTL_CONFIG_IO_HANDLER:
            dump_config_io_handler_in(ioctl.config_io_handler());
            break;

        case cng::CngIoctlIn::IOCTL_KSEC_EX_REGISTER_EXTENSION:
            printf("IOCTL_KSEC_EX_REGISTER_EXTENSION: extension_type = 0x%08x\n", ioctl.ksec_ex_register_extension().extension_type());
            if ( ioctl.ksec_ex_register_extension().has_extension_table_32() )
            {
                printf("IOCTL_KSEC_EX_REGISTER_EXTENSION: extension_table = 0x%08x\n", ioctl.ksec_ex_register_extension().extension_table_32());
            }
            else if ( ioctl.ksec_ex_register_extension().has_extension_table_64() )
            {
                printf("IOCTL_KSEC_EX_REGISTER_EXTENSION: extension_table = 0x%08x%08x\n",
                    (uint32_t)(ioctl.ksec_ex_register_extension().extension_table_64() >> 32),
                    (uint32_t)(ioctl.ksec_ex_register_extension().extension_table_64() >> 0)
                );
            }
            else
            {
                printf("IOCTL_KSEC_EX_REGISTER_EXTENSION: (invalid pointer size)\n");
            }
            break;

        case cng::CngIoctlIn::IOCTL_KSEC_EX_REMOVE_VALID_VM:
            if ( ioctl.ksec_ex_remove_valid_vm().has_pointer32() )
            {
                printf("IOCTL_KSEC_EX_REMOVE_VALID_VM: pointer = 0x%08x\n", ioctl.ksec_ex_remove_valid_vm().pointer32());
            }
            else if ( ioctl.ksec_ex_remove_valid_vm().has_pointer64() )
            {
                printf("IOCTL_KSEC_EX_REMOVE_VALID_VM: pointer = 0x%08x%08x\n",
                    (uint32_t)(ioctl.ksec_ex_remove_valid_vm().pointer64() >> 32),
                    (uint32_t)(ioctl.ksec_ex_remove_valid_vm().pointer64() >> 0)
                );
            }
            else
            {
                printf("IOCTL_KSEC_EX_REMOVE_VALID_VM: (invalid pointer size)\n");
            }
            break;

        case cng::CngIoctlIn::IOCTL_IPC_GET_QUEUED_FUNCTION_CALLS:
            printf("IOCTL_IPC_GET_QUEUED_FUNCTION_CALLS\n");
            break;

        case cng::CngIoctlIn::IOCTL_IPC_HANDLE_FUNCTION_RETURN:
            if ( ioctl.ipc_handle_function_return().has_call_id_32() )
            {
                printf("IOCTL_IPC_HANDLE_FUNCTION_RETURN: pointer = 0x%08x\n", ioctl.ipc_handle_function_return().call_id_32());
            }
            else if ( ioctl.ipc_handle_function_return().has_call_id_64() )
            {
                printf("IOCTL_IPC_HANDLE_FUNCTION_RETURN: pointer = 0x%08x%08x\n",
                    (uint32_t)(ioctl.ipc_handle_function_return().call_id_64() >> 32),
                    (uint32_t)(ioctl.ipc_handle_function_return().call_id_64() >> 0)
                );
            }
            else
            {
                printf("IOCTL_IPC_HANDLE_FUNCTION_RETURN: (invalid pointer size)\n");
            }
            printf("IOCTL_IPC_HANDLE_FUNCTION_RETURN: status = 0x%08x\n", ioctl.ipc_handle_function_return().status());
            break;

        case cng::CngIoctlIn::IOCTL_KSECDD_SYSTEM_PROCESS:
            printf("IOCTL_KSECDD_SYSTEM_PROCESS\n");
            break;

        default:
            printf("(unknown ioctl 0x%08x)\n", ioctl.ioctl());
            break;
    }
}

void dump_ioctl_out(const cng::CngIoctlOut &ioctl)
{
    if ( ioctl.ioctl() == 0 )
    {
        printf("CNG IOCTL (OUT): (ioctl code not set)\n");
        return;
    }
    printf("CNG IOCTL 0x%08x (OUT):\n", ioctl.ioctl());
    switch ( ioctl.ioctl() )
    {
        case cng::CngIoctlOut::IOCTL_SYSTEM_PRNG:
            dump_system_prng(ioctl.system_prng());
            break;

        case cng::CngIoctlOut::IOCTL_SYSTEM_PRNG_2:
            dump_system_prng(ioctl.system_prng_2());
            break;

        case cng::CngIoctlOut::IOCTL_ENCRYPT_MEMORY_PROCESS:
            dump_encrypt_memory(ioctl.encrypt_memory_process());
            break;

        case cng::CngIoctlOut::IOCTL_DECRYPT_MEMORY_PROCESS:
            dump_encrypt_memory(ioctl.decrypt_memory_process());
            break;

        case cng::CngIoctlOut::IOCTL_ENCRYPT_MEMORY_GLOBAL:
            dump_encrypt_memory(ioctl.encrypt_memory_global());
            break;

        case cng::CngIoctlOut::IOCTL_DECRYPT_MEMORY_GLOBAL:
            dump_encrypt_memory(ioctl.decrypt_memory_global());
            break;

        case cng::CngIoctlOut::IOCTL_ENCRYPT_MEMORY_AUTH_ID:
            dump_encrypt_memory(ioctl.encrypt_memory_auth_id());
            break;

        case cng::CngIoctlOut::IOCTL_DECRYPT_MEMORY_AUTH_ID:
            dump_encrypt_memory(ioctl.decrypt_memory_auth_id());
            break;

        case cng::CngIoctlOut::IOCTL_ENCRYPT_MEMORY_AUTH_ID_994:
            dump_encrypt_memory(ioctl.encrypt_memory_auth_id());
            break;

        case cng::CngIoctlOut::IOCTL_DECRYPT_MEMORY_AUTH_ID_994:
            dump_encrypt_memory(ioctl.decrypt_memory_auth_id());
            break;

        case cng::CngIoctlOut::IOCTL_CONFIG_IO_HANDLER:
            dump_config_io_handler_out(ioctl.config_io_handler());
            break;

        case cng::CngIoctlOut::IOCTL_SELF_TEST_DURATION:
            printf("SELF TEST DURATION (OUT): 0x%08x%08x\n",
                (uint32_t)(ioctl.self_test_duration().self_test_duration() >> 32),
                (uint32_t)(ioctl.self_test_duration().self_test_duration() >> 0));
            break;

        case cng::CngIoctlOut::IOCTL_KSEC_EX_REGISTER_EXTENSION:
            printf("IOCTL_KSEC_EX_REGISTER_EXTENSION\n");
            break;

        case cng::CngIoctlOut::IOCTL_KSEC_EX_REMOVE_VALID_VM:
            printf("IOCTL_KSEC_EX_REMOVE_VALID_VM\n");
            break;

        case cng::CngIoctlOut::IOCTL_IPC_GET_QUEUED_FUNCTION_CALLS:
            printf("IOCTL_IPC_GET_QUEUED_FUNCTION_CALLS\n");
            break;

        case cng::CngIoctlOut::IOCTL_IPC_HANDLE_FUNCTION_RETURN:
            printf("IOCTL_IPC_HANDLE_FUNCTION_RETURN\n");
            break;

        case cng::CngIoctlOut::IOCTL_KSECDD_SYSTEM_PROCESS:
            printf("IOCTL_KSECDD_SYSTEM_PROCESS: process_id=%d\n", ioctl.ksecdd_system_process().process_id());
            break;

        default:
            printf("(unknown ioctl 0x%08x)\n", ioctl.ioctl());
            break;
    }
}

};
