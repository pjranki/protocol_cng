meta:
  id: ksec_ex_remove_valid_vm
  endian: le

doc: |
  This represents the virtual memory address to be removed by ksecdd.sys on ioctl code 0x00390058

params:
  - id: pointer_size
    type: u1
    -default: 8

seq:
  - id: pointer32
    type: u4
    if: pointer_size == 4
    -set:
      - id: pointer64
        value: pointer32
  - id: pointer64
    type: u8
    if: pointer_size == 8
    -set:
      - id: pointer32
        value: pointer64
