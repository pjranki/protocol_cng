meta:
  id: kernel_pointer

doc: |
  NOTE: This is KERNEL MODE only
  cng!CngDeviceControl
  cng!CryptIoctlReturnKernelmodePointer

seq:
  - id: data
    size-eos: true
