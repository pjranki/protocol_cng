meta:
  id: crypt_property_ref
  -orig-id: CRYPT_PROPERTY_REF
  endian: le
  imports:
    - io_string_ptr
    - io_buffer

params:
  - id: pointer_size
    type: u1
    -default: 8  # by default, assume 64-bit pointers/offsets

seq:
  - id: property
    -orig-id: pszProperty
    type: io_string_ptr(pointer_size)
  - id: value
    type: io_buffer(pointer_size)
