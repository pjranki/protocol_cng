meta:
  id: io_configuration_paramblock
  -orig-id: IO_CONFIGURATION_PARAMBLOCK
  endian: le
  imports:
    - io_string_ptr
    - crypt_context_config_ptr
    - crypt_context_function_config_ptr
    - io_buffer

params:
  - id: pointer_size
    type: u1
    -default: 8  # by default, assume 64-bit pointers/offsets

seq:
  - id: table
    -orig-id: dwTable
    type: u4
  - id: padding1
    size: 'pointer_size == 8 ? 4 : 0'
  - id: context
    -orig-id: pszContext
    type: io_string_ptr(pointer_size)
  - id: interface
    -orig-id: dwInterface
    type: u4
  - id: padding2
    size: 'pointer_size == 8 ? 4 : 0'
  - id: function
    -orig-id: pszFunction
    type: io_string_ptr(pointer_size)
  - id: provider
    -orig-id: pszProvider
    type: io_string_ptr(pointer_size)
  - id: property
    -orig-id: pszProperty
    type: io_string_ptr(pointer_size)
  - id: position
    -orig-id: dwPosition
    type: u4
  - id: padding3
    size: 'pointer_size == 8 ? 4 : 0'
  - id: context_config
    -orig-id: pContextConfig
    type: crypt_context_config_ptr(pointer_size)
  - id: context_function_config
    -orig-id: pContextFunctionConfig
    type: crypt_context_function_config_ptr(pointer_size)
  - id: value
    type: io_buffer
  - id: event32
    type: u4
    if: pointer_size == 4
    -default: 0
    -set:
      - id: event64
        value: event32
  - id: event64
    type: u8
    if: pointer_size == 8
    -default: 0
    -set:
      - id: event32
        value: event64

instances:
  event:
    value: '(pointer_size == 4) ? event32 : event64'

enums:
  crypt_interface:
    1:
      id: cipher
      -orig-id: BCRYPT_CIPHER_INTERFACE
    2:
      id: hash
      -orig-id: BCRYPT_HASH_INTERFACE
    3:
      id: asymmetric_encryption
      -orig-id: BCRYPT_ASYMMETRIC_ENCRYPTION_INTERFACE
    4:
      id: secret_agreement
      -orig-id: BCRYPT_SECRET_AGREEMENT_INTERFACE
    5:
      id: signature
      -orig-id: BCRYPT_SIGNATURE_INTERFACE
    6:
      id: rng
      -orig-id: BCRYPT_RNG_INTERFACE
    7:
      id: key_derivation
      -orig-id: BCRYPT_KEY_DERIVATION_INTERFACE
    0x10001:
      id: key_storage
      -orig-id: NCRYPT_KEY_STORAGE_INTERFACE
    0x10002:
      id: schannel
      -orig-id: NCRYPT_SCHANNEL_INTERFACE
    0x10003:
      id: schannel_signature
      -orig-id: NCRYPT_SCHANNEL_SIGNATURE_INTERFACE
    0x10004:
      id: key_protection
      -orig-id: NCRYPT_KEY_PROTECTION_INTERFACE

  crypt_table:
    1:
      id: local
      -orig-id: CRYPT_LOCAL
    2:
      id: domain
      -orig-id: CRYPT_DOMAIN
