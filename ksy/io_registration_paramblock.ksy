meta:
  id: io_registration_paramblock
  -orig-id: IO_REGISTRATION_PARAMBLOCK
  endian: le
  imports:
    - io_string_ptr
    - crypt_provider_reg_ptr

params:
  - id: pointer_size
    type: u1
    -default: 8  # by default, assume 64-bit pointers/offsets

seq:
  - id: provider
    -orig-id: pszProvider
    type: io_string_ptr(pointer_size)
  - id: mode
    -orig-id: dwMode
    type: u4
  - id: interface
    -orig-id: dwInterface
    type: u4
  - id: flags
    -orig-id: dwFlags
    type: u4
  - id: padding
    size: 'pointer_size == 8 ? 4 : 0'
  - id: reg
    -orig-id: pReg
    type: crypt_provider_reg_ptr(pointer_size)

enums:
  crypt_interface:
    0:
      id: all
    1:
      id: cipher
      -orig-id: BCRYPT_CIPHER_INTERFACE
    2:
      id: hash
      -orig-id: BCRYPT_HASH_INTERFACE
    3:
      id: asymmetric_encryption
      -orig-id: BCRYPT_ASYMMETRIC_ENCRYPTION_INTERFACE
    4:
      id: secret_agreement
      -orig-id: BCRYPT_SECRET_AGREEMENT_INTERFACE
    5:
      id: signature
      -orig-id: BCRYPT_SIGNATURE_INTERFACE
    6:
      id: rng
      -orig-id: BCRYPT_RNG_INTERFACE
    7:
      id: key_derivation
      -orig-id: BCRYPT_KEY_DERIVATION_INTERFACE
    0x10001:
      id: key_storage
      -orig-id: NCRYPT_KEY_STORAGE_INTERFACE
    0x10002:
      id: schannel
      -orig-id: NCRYPT_SCHANNEL_INTERFACE
    0x10003:
      id: schannel_signature
      -orig-id: NCRYPT_SCHANNEL_SIGNATURE_INTERFACE
    0x10004:
      id: key_protection
      -orig-id: NCRYPT_KEY_PROTECTION_INTERFACE

  crypt_mode:
    1:
      id: um
      -orig-id: CRYPT_UM
    2:
      id: km
      -orig-id: CRYPT_KM
    3:
      id: mm
      -orig-id: CRYPT_MM
    4:
      id: any
      -orig-id: CRYPT_ANY
