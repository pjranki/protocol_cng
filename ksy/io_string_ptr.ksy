meta:
  id: io_string_ptr
  endian: le

params:
  - id: pointer_size
    type: u1
    -default: 8  # by default, assume 64-bit pointers/offsets

seq:
  - id: offset32
    type: u4
    if: pointer_size == 4
    -default: 0xffffffff
    -set:
      - id: offset64
        value: offset32
  - id: offset64
    type: u8
    if: pointer_size == 8
    -default: 0xffffffffffffffff
    -set:
      - id: offset32
        value: offset64

instances:
  offset:
    value: '(pointer_size == 4) ? offset32 : offset64'
  valid_offset:
    value: '(pointer_size == 4) ? (offset32 != 0xffffffff) : (offset64 != 0xffffffffffffffff)'
  value:
    pos: offset
    io: _root._io
    type: strz
    encoding: utf16le
    if: valid_offset
