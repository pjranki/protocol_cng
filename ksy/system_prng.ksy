meta:
  id: system_prng

doc: |
  cng!CngDeviceControl
  cng!SystemPrng

seq:
  - id: data
    size-eos: true
