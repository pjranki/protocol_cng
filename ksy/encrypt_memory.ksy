meta:
  id: encrypt_memory

doc: |
  cng!CngDeviceControl
  cng!CngEncryptMemoryEx

params:
  - id: encrypt
    type: bool
  - id: option
    type: u4
    enum: encrypt_option
  - id: output_size
    type: u4

seq:
  - id: data
    size-eos: true

instances:
  output_data_size:
    value: output_size
  cipher:
    value: '((output_data_size & 0x7) != 0 ? cipher::error.to_i : ((output_data_size & 0xf) != 0 ? cipher::cbc.to_i : cipher::aes.to_i))'

-instances:
  output_data_size:  # data.length is not supported in kaitai when using 'size-eos: true'
    value: '(output_size  != 0 ? output_size : data.length) & 0xffffffff'

enums:
  encrypt_option:
    0: process
    1: global
    2: auth_id
    4: auth_id_994

  cipher:
    0: error
    1: cbc
    2: aes
