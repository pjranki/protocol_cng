meta:
  id: cng_ioctl_out
  endian: le
  imports:
    - system_prng
    - encrypt_memory
    - fips_function_table
    - kernel_pointer
    - self_test_duration
    - config_io_handler_out
    - ksecdd_system_process

doc: |
  cng!CngDeviceControl

params:
  - id: pointer_size
    type: u1
    -default: 8
  - id: ioctl
    type: u4
  - id: function
    type: u4

seq:
  - id: handler
    -set:
      - id: ioctl
        value: _case
    type:
      switch-on: ioctl
      -name: enum
      cases:
        'ioctl::system_prng.to_i': system_prng
        'ioctl::system_prng_2.to_i': system_prng
        'ioctl::encrypt_memory_process.to_i': encrypt_memory(true, key::process.to_i, 0)
        'ioctl::decrypt_memory_process.to_i': encrypt_memory(false, key::process.to_i, 0)
        'ioctl::encrypt_memory_global.to_i': encrypt_memory(true, key::global.to_i, 0)
        'ioctl::decrypt_memory_global.to_i': encrypt_memory(false, key::global.to_i, 0)
        'ioctl::encrypt_memory_auth_id.to_i': encrypt_memory(true, key::auth_id.to_i, 0)
        'ioctl::decrypt_memory_auth_id.to_i': encrypt_memory(false, key::auth_id.to_i, 0)
        'ioctl::encrypt_memory_auth_id_994.to_i': encrypt_memory(true, key::auth_id_994.to_i, 0)
        'ioctl::decrypt_memory_auth_id_994.to_i': encrypt_memory(false, key::auth_id_994.to_i, 0)
        'ioctl::fips_function_table.to_i': fips_function_table
        'ioctl::ksec_ex_bcrypt_extension.to_i': kernel_pointer
        'ioctl::ksec_ex_ssl_extension.to_i': kernel_pointer
        'ioctl::ksec_ex_device_control_extension.to_i': kernel_pointer
        'ioctl::ksec_ex_device_extension_2.to_i': kernel_pointer
        'ioctl::se_audit_fips_crypto_self_tests_0.to_i': empty_ioctl_out
        'ioctl::se_audit_fips_crypto_self_tests_1.to_i': empty_ioctl_out
        'ioctl::self_test_duration.to_i': self_test_duration
        'ioctl::config_io_handler.to_i': config_io_handler_out(pointer_size, function)

        'ioctl::ksec_ex_remove_valid_vm.to_i': empty_ioctl_out
        'ioctl::ipc_get_queued_function_calls.to_i': empty_ioctl_out
        'ioctl::ipc_handle_function_return.to_i': empty_ioctl_out
        'ioctl::ksecdd_system_process.to_i': ksecdd_system_process

enums:
  ioctl:
    0x00390004: system_prng
    0x00390008: system_prng_2
    0x0039000e: encrypt_memory_process
    0x00390012: decrypt_memory_process
    0x00390016: encrypt_memory_global
    0x0039001a: decrypt_memory_global
    0x0039001e: encrypt_memory_auth_id
    0x00390022: decrypt_memory_auth_id
    0x0039007a: encrypt_memory_auth_id_994
    0x0039007e: decrypt_memory_auth_id_994
    0x00390024: fips_function_table
    0x00390038: ksec_ex_register_extension              # ksecdd.sys
    0x00390040: ksec_ex_bcrypt_extension
    0x00390044: ksec_ex_ssl_extension
    0x00390048: ksec_ex_device_control_extension
    0x00390058: ksec_ex_remove_valid_vm                 # ksecdd.sys
    0x00390064: ksec_ex_device_extension_2
    0x0039006a: ipc_get_queued_function_calls           # ksecdd.sys
    0x0039006f: ipc_handle_function_return              # ksecdd.sys
    0x00390073: se_audit_fips_crypto_self_tests_0
    0x00390074: se_audit_fips_crypto_self_tests_1
    0x00390084: self_test_duration
    0x00390400: config_io_handler
    0x00398000: ksecdd_system_process                   # ksecdd.sys

  key:
    0: process
    1: global
    2: auth_id
    4: auth_id_994

types:
  empty_ioctl_out:
    seq:
      - id: data
        size-eos: true
