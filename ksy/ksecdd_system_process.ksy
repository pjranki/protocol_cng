meta:
  id: ksecdd_system_process
  endian: le

doc: |
  This represents the process ID returned by ksecdd.sys on ioctl code 0x00398000

seq:
  - id: process_id
    type: u4
