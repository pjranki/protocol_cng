meta:
  id: self_test_duration
  endian: le

doc: |
  cng!CngDeviceControl

seq:
  - id: self_test_duration
    type: u8
