meta:
  id: crypt_property_refs
  -orig-id: CRYPT_PROPERTY_REFS
  endian: le
  imports:
    - crypt_property_ref_ptr

params:
  - id: pointer_size
    type: u1
    -default: 8  # by default, assume 64-bit pointers/offsets

seq:
  - id: count
    type: u4
  - id: padding
    size: 'pointer_size == 8 ? 4 : 0'
  - id: offset32
    type: u4
    if: pointer_size == 4
    -default: 0xffffffff
    -set:
      - id: offset64
        value: offset32
  - id: offset64
    type: u8
    if: pointer_size == 8
    -default: 0xffffffffffffffff
    -set:
      - id: offset32
        value: offset64

-update:
  - id: count
    value: array.size

instances:
  offset:
    value: 'pointer_size == 4 ? offset32 : offset64'
  valid_offset:
    value: '(pointer_size == 4) ? (offset32 != 0xffffffff) : (offset64 != 0xffffffffffffffff)'
  array:
    type: crypt_property_ref_ptr(pointer_size)
    io: _root._io
    pos: offset
    repeat: expr
    repeat-expr: count
    if: valid_offset
