meta:
  id: io_resolution_paramblock
  -orig-id: IO_RESOLUTION_PARAMBLOCK
  endian: le
  imports:
    - io_string_ptr

params:
  - id: pointer_size
    type: u1
    -default: 8  # by default, assume 64-bit pointers/offsets

seq:
  - id: context
    -orig-id: pszContext
    type: io_string_ptr(pointer_size)
  - id: interface
    -orig-id: dwInterface
    type: u4
  - id: padding
    size: 'pointer_size == 8 ? 4 : 0'
  - id: function
    -orig-id: pszFunction
    type: io_string_ptr(pointer_size)
  - id: provider
    -orig-id: pszProvider
    type: io_string_ptr(pointer_size)
  - id: mode
    -orig-id: dwMode
    type: u4
  - id: flags
    -orig-id: dwFlags
    type: u4

enums:
  crypt_interface:
    1:
      id: cipher
      -orig-id: BCRYPT_CIPHER_INTERFACE
    2:
      id: hash
      -orig-id: BCRYPT_HASH_INTERFACE
    3:
      id: asymmetric_encryption
      -orig-id: BCRYPT_ASYMMETRIC_ENCRYPTION_INTERFACE
    4:
      id: secret_agreement
      -orig-id: BCRYPT_SECRET_AGREEMENT_INTERFACE
    5:
      id: signature
      -orig-id: BCRYPT_SIGNATURE_INTERFACE
    6:
      id: rng
      -orig-id: BCRYPT_RNG_INTERFACE
    7:
      id: key_derivation
      -orig-id: BCRYPT_KEY_DERIVATION_INTERFACE
    0x10001:
      id: key_storage
      -orig-id: NCRYPT_KEY_STORAGE_INTERFACE
    0x10002:
      id: schannel
      -orig-id: NCRYPT_SCHANNEL_INTERFACE
    0x10003:
      id: schannel_signature
      -orig-id: NCRYPT_SCHANNEL_SIGNATURE_INTERFACE
    0x10004:
      id: key_protection
      -orig-id: NCRYPT_KEY_PROTECTION_INTERFACE

  crypt_mode:
    1:
      id: um
      -orig-id: CRYPT_UM
    2:
      id: km
      -orig-id: CRYPT_KM
    3:
      id: mm
      -orig-id: CRYPT_MM
    4:
      id: any
      -orig-id: CRYPT_ANY

  crypt_flag:
    1:
      id: all_functions
      -orig-id: CRYPT_ALL_FUNCTIONS
    2:
      id: all_providers
      -orig-id: CRYPT_ALL_PROVIDERS
