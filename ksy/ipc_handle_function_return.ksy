meta:
  id: ipc_handle_function_return
  -orig-id: _KsecIoctlIpcReturnValue
  endian: le

doc: |
  This represents the buffer passed to ksecdd.sys on ioctl code 0x0039006f

params:
  - id: pointer_size
    type: u1
    -default: 8

seq:
  - id: call_id_32
    type: u4
    if: pointer_size == 4
    -set:
      - id: call_id_64
        value: call_id_32
  - id: call_id_64
    type: u8
    if: pointer_size == 8
    -set:
      - id: call_id_32
        value: call_id_64
  - id: status
    type: u4
  - id: padding
    size: 'pointer_size == 8 ? 4 : 0'
