meta:
  id: crypt_provider_reg
  -orig-id: CRYPT_PROVIDER_REG
  endian: le
  imports:
    - io_string_array
    - crypt_image_reg_ptr

params:
  - id: pointer_size
    type: u1
    -default: 8  # by default, assume 64-bit pointers/offsets

seq:
  - id: aliases
    type: io_string_array(pointer_size)
  - id: um
    -orig-id: pUM
    type: crypt_image_reg_ptr(pointer_size)
  - id: km
    -orig-id: pKM
    type: crypt_image_reg_ptr(pointer_size)
