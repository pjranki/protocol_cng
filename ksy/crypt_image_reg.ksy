meta:
  id: crypt_image_reg
  -orig-id: CRYPT_IMAGE_REG
  endian: le
  imports:
    - io_string_ptr
    - crypt_interface_regs

params:
  - id: pointer_size
    type: u1
    -default: 8  # by default, assume 64-bit pointers/offsets

seq:
  - id: image
    -orig-id: pszImage
    type: io_string_ptr(pointer_size)
  - id: interfaces
    type: crypt_interface_regs(pointer_size)
