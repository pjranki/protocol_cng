meta:
  id: io_buffer
  endian: le

params:
  - id: pointer_size
    type: u1
    -default: 8  # by default, assume 64-bit pointers/offsets

seq:
  - id: value_size
    -orig-id: cbValue
    type: u4
  - id: padding
    size: 'pointer_size == 8 ? 4 : 0'
  - id: offset32
    type: u4
    if: pointer_size == 4
    -default: 0xffffffff
    -set:
      - id: offset64
        value: offset32
  - id: offset64
    type: u8
    if: pointer_size == 8
    -default: 0xffffffffffffffff
    -set:
      - id: offset32
        value: offset64

-update:
  - id: value_size
    value: value.length

instances:
  offset:
    value: 'pointer_size == 4 ? offset32 : offset64'
  valid_offset:
    value: '(pointer_size == 4) ? (offset32 != 0xffffffff) : (offset64 != 0xffffffffffffffff)'
  value:
    size: value_size
    io: _root._io
    pos: offset
    if: valid_offset
