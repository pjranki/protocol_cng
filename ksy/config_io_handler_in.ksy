meta:
  id: config_io_handler_in
  -orig-id: IO_PARAMBLOCK_HEADER
  endian: le
  imports:
    - io_registration_paramblock
    - io_configuration_paramblock
    - io_resolution_paramblock

doc: |
  cng!CngDeviceControl
  cng!ConfigIoHandler_Safeguarded
  cng!IoUnpack_SG_ParamBlock_Header
  cng!_ConfigFunctionIoHandler

params:
  - id: pointer_size
    type: u1
    -default: 8

seq:
  - id: magic
    type: u4
    -default: 0x1A2B3C4D
  - id: function
    type: u4
  - id: paramblock
    -set:
      - id: function
        value: _case
    type:
      switch-on: function
      -name: enum
      cases:
        'function::bcrypt_register_provider.to_i': io_registration_paramblock
        'function::bcrypt_unregister_provider.to_i': io_registration_paramblock
        'function::bcrypt_query_provider_registeration.to_i': io_registration_paramblock
        'function::bcrypt_enum_registered_providers.to_i': io_registration_paramblock
        'function::bcrypt_create_context.to_i': io_configuration_paramblock
        'function::bcrypt_delete_context.to_i': io_configuration_paramblock
        'function::bcrypt_enum_contexts.to_i': io_configuration_paramblock
        'function::bcrypt_configure_context.to_i': io_configuration_paramblock
        'function::bcrypt_query_context_configuration.to_i': io_configuration_paramblock
        'function::bcrypt_add_context_function.to_i': io_configuration_paramblock
        'function::bcrypt_remove_context_function.to_i': io_configuration_paramblock
        'function::bcrypt_enum_context_functions.to_i': io_configuration_paramblock
        'function::bcrypt_configure_context_function.to_i': io_configuration_paramblock
        'function::bcrypt_query_context_function_configuration.to_i': io_configuration_paramblock
        'function::bcrypt_add_context_function_provider.to_i': io_configuration_paramblock
        'function::bcrypt_remove_context_function_provider.to_i': io_configuration_paramblock
        'function::bcrypt_enum_context_function_providers.to_i': io_configuration_paramblock
        'function::bcrypt_set_context_function_property.to_i': io_configuration_paramblock
        'function::bcrypt_query_context_function_property.to_i': io_configuration_paramblock
        'function::client_register_config_change_notify.to_i': io_configuration_paramblock
        'function::client_unregister_config_change_notify.to_i': io_configuration_paramblock
        'function::bcrypt_resolve_providers.to_i': io_resolution_paramblock
        _: config_io_raw_in

enums:
  function:
    0x00000000: bcrypt_register_provider
    0x00000001: bcrypt_unregister_provider
    0x00000002: bcrypt_query_provider_registeration
    0x00000003: bcrypt_enum_registered_providers
    0x00010000: bcrypt_create_context
    0x00010001: bcrypt_delete_context
    0x00010002: bcrypt_enum_contexts
    0x00010003: bcrypt_configure_context
    0x00010004: bcrypt_query_context_configuration
    0x00010200: bcrypt_add_context_function
    0x00010201: bcrypt_remove_context_function
    0x00010202: bcrypt_enum_context_functions
    0x00010203: bcrypt_configure_context_function
    0x00010204: bcrypt_query_context_function_configuration
    0x00010300: bcrypt_add_context_function_provider
    0x00010301: bcrypt_remove_context_function_provider
    0x00010302: bcrypt_enum_context_function_providers
    0x00010400: bcrypt_set_context_function_property
    0x00010401: bcrypt_query_context_function_property
    0x00010500: client_register_config_change_notify
    0x00010501: client_unregister_config_change_notify
    0x00020000: bcrypt_resolve_providers

types:
  config_io_raw_in:
    seq:
      - id: data
        size-eos: true
