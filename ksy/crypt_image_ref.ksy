meta:
  id: crypt_image_ref
  -orig-id: CRYPT_IMAGE_REF
  endian: le
  imports:
    - io_string_ptr

params:
  - id: pointer_size
    type: u1
    -default: 8  # by default, assume 64-bit pointers/offsets

seq:
  - id: image
    -orig-id: pszImage
    type: io_string_ptr(pointer_size)
  - id: flags
    -orig-id: dwFlags
    type: u4
  - id: padding
    size: 'pointer_size == 8 ? 4 : 0'
