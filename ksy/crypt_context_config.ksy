meta:
  id: crypt_context_config
  -orig-id: CRYPT_CONTEXT_CONFIG
  endian: le

seq:
  - id: flags
    -orig-id: dwFlags
    type: u4
  - id: reserved
    -orig-id: dwReserved
    type: u4

enums:
  crypt_flag:
    1:
      id: exclusive
      -orig-id: CRYPT_EXCLUSIVE
    0x00010000:
      id: override
      -orig-id: CRYPT_OVERRIDE
