meta:
  id: ksec_extension_registration
  -orig-id: KSEC_EXTENSION_REGISTRATION
  endian: le

doc: |
  This represents the buffer passed to ksecdd.sys on ioctl code 0x00390038

params:
  - id: pointer_size
    type: u1
    -default: 8

seq:
  - id: extension_type
    -orig-id: ExtensionType
    type: u4
    enum: ksec
  - id: padding
    size: 'pointer_size == 8 ? 4 : 0'
  - id: extension_table_32
    type: u4
    if: pointer_size == 4
    -set:
      - id: extension_table_64
        value: extension_table_32
  - id: extension_table_64
    type: u8
    if: pointer_size == 8
    -set:
      - id: extension_table_32
        value: extension_table_64

enums:
  ksec:
    0x0:
      id: bcrypt_extension
      -orig-id: KsecBCryptExtension
    0x1:
      id: ssl_extension
      -orig-id: KsecSslExtension
    0x2:
      id: auth_packages_extension
      -orig-id: KsecAuthPackagesExtension
    0x3:
      id: device_control_extension
      -orig-id: KsecDeviceControlExtension
    0x4:
      id: miscellaneous_extension
      -orig-id: KsecMiscellaneousExtension
