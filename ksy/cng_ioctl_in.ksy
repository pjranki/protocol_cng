meta:
  id: cng_ioctl_in
  endian: le
  imports:
    - system_prng
    - encrypt_memory
    - self_test_duration
    - config_io_handler_in
    - ksec_extension_registration
    - ksec_ex_remove_valid_vm
    - ipc_handle_function_return

doc: |
  cng!CngDeviceControl

params:
  - id: pointer_size
    type: u1
    -default: 8
  - id: output_size
    type: u4
  - id: ioctl
    type: u4

seq:
  - id: handler
    -set:
      - id: ioctl
        value: _case
    type:
      switch-on: ioctl
      -name: enum
      cases:
        'ioctl::system_prng.to_i': system_prng
        'ioctl::system_prng_2.to_i': system_prng
        'ioctl::encrypt_memory_process.to_i': encrypt_memory(true, key::process.to_i, output_size)
        'ioctl::decrypt_memory_process.to_i': encrypt_memory(false, key::process.to_i, output_size)
        'ioctl::encrypt_memory_global.to_i': encrypt_memory(true, key::global.to_i, output_size)
        'ioctl::decrypt_memory_global.to_i': encrypt_memory(false, key::global.to_i, output_size)
        'ioctl::encrypt_memory_auth_id.to_i': encrypt_memory(true, key::auth_id.to_i, output_size)
        'ioctl::decrypt_memory_auth_id.to_i': encrypt_memory(false, key::auth_id.to_i, output_size)
        'ioctl::encrypt_memory_auth_id_994.to_i': encrypt_memory(true, key::auth_id_994.to_i, output_size)
        'ioctl::decrypt_memory_auth_id_994.to_i': encrypt_memory(false, key::auth_id_994.to_i, output_size)
        'ioctl::fips_function_table.to_i': empty_ioctl_in
        'ioctl::ksec_ex_bcrypt_extension.to_i': empty_ioctl_in
        'ioctl::ksec_ex_ssl_extension.to_i': empty_ioctl_in
        'ioctl::ksec_ex_device_control_extension.to_i': empty_ioctl_in
        'ioctl::ksec_ex_device_extension_2.to_i': empty_ioctl_in
        'ioctl::se_audit_fips_crypto_self_tests_0.to_i': empty_ioctl_in
        'ioctl::se_audit_fips_crypto_self_tests_1.to_i': empty_ioctl_in
        'ioctl::self_test_duration.to_i': self_test_duration
        'ioctl::config_io_handler.to_i': config_io_handler_in(pointer_size)
        'ioctl::ksec_ex_register_extension.to_i': ksec_extension_registration(pointer_size)  # ksecdd.sys
        'ioctl::ksec_ex_remove_valid_vm.to_i': ksec_ex_remove_valid_vm(pointer_size)        # ksecdd.sys
        'ioctl::ipc_get_queued_function_calls.to_i': empty_ioctl_in                         # ksecdd.sys
        'ioctl::ipc_handle_function_return.to_i': ipc_handle_function_return                # ksecdd.sys
        'ioctl::ksecdd_system_process.to_i': empty_ioctl_in                                 # ksecdd.sys

enums:
  ioctl:
    0x00390004: system_prng
    0x00390008: system_prng_2
    0x0039000e: encrypt_memory_process
    0x00390012: decrypt_memory_process
    0x00390016: encrypt_memory_global
    0x0039001a: decrypt_memory_global
    0x0039001e: encrypt_memory_auth_id
    0x00390022: decrypt_memory_auth_id
    0x0039007a: encrypt_memory_auth_id_994
    0x0039007e: decrypt_memory_auth_id_994
    0x00390024: fips_function_table
    0x00390038: ksec_ex_register_extension              # ksecdd.sys
    0x00390040: ksec_ex_bcrypt_extension
    0x00390044: ksec_ex_ssl_extension
    0x00390048: ksec_ex_device_control_extension
    0x00390058: ksec_ex_remove_valid_vm                 # ksecdd.sys
    0x00390064: ksec_ex_device_extension_2
    0x0039006a: ipc_get_queued_function_calls           # ksecdd.sys
    0x0039006f: ipc_handle_function_return              # ksecdd.sys
    0x00390073: se_audit_fips_crypto_self_tests_0
    0x00390074: se_audit_fips_crypto_self_tests_1
    0x00390084: self_test_duration
    0x00390400: config_io_handler
    0x00398000: ksecdd_system_process                   # ksecdd.sys

  key:
    0: process
    1: global
    2: auth_id
    4: auth_id_994

types:
  empty_ioctl_in:
    seq:
      - id: data
        size-eos: true
